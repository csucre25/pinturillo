'use strict';

var express = require('express');
var router = express.Router();
var controller = require('../controllers/riot');

router.post('/:manager', controller.manageRiot);

module.exports = router;