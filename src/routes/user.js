'use strict';

var express = require('express');
var router  = express.Router();
var controller = require('../controllers/user');

router.post('/:manager', controller.manageUser);

module.exports = router;