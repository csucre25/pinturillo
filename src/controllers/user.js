'use strict';
var firebase = require('firebase');
var admin = require("firebase-admin");
var request = require("request");
const uuidv4 = require("uuid");
var serviceAccount = require("../pinturillov2-firebase-adminsdk-7ag89-54c3265fc7.json");
firebase.initializeApp({

    databaseURL: "https://pinturillov2-default-rtdb.firebaseio.com/",

    credential: admin.credential.cert(serviceAccount)

});

exports.manageUser = async(req,res)=>{
    var manager = req.params.manager;
    switch (manager) {
        case 'create':
            return createUser(req,res);
            break;
        default:
            return false;
            break;
    }
};

var createUser = async (req,res)=>{
    var url_Icon = req.body.url_Icon;
    var user_name = req.body.user_name;
    var room_code = req.body.room_code;
    var option = req.body.option;
    var capacidad = req.body.capacidad;
    console.log(req.body);
    //var sala = null;
    //var userId = "test";
//    var salas = req.app.get('salas');
    console.log(salas);
    var idsala = 0;
    var uniqueid = uuidv4.v4();
    if (option == 'Created') {
        const sala = salas.getMaxRoom();
        if (sala) {
            idsala = sala.id+1;
            salas.addRoom(idsala,capacidad);
        } else {
            idsala = 1;
            salas.addRoom(idsala,capacidad);
        }
    }else{
        //validamos que exista la sala
    }
    var actsalas = salas.getRooms();
    console.log(actsalas);
    await firebase.database().ref('users/' + uniqueid).set({
        url_Icon: url_Icon,
        user_name: user_name,
        room_code: room_code
    }, (error) => {
        if (error) {
            return res.send(err)
        } else {
            return res.json({ message: "Success: User Save.", result: true, uniqueid: uniqueid });
        }
    });
};