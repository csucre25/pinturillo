'use strict';
var firebase = require('firebase');
var admin = require("firebase-admin");
var request = require("request");
const uuidv4 = require("uuid");
const bodyParser = require('body-parser');

//Constants for riot api
const apiRiot = "https://ddragon.leagueoflegends.com/cdn/";
const apiVersions = "https://ddragon.leagueoflegends.com/api/versions.json";
const apiLanguaje = "https://ddragon.leagueoflegends.com/cdn/languages.json";
const apiData = "/data/";

//Use: apiRiot + version + apiData + languaje + "/champion.json"
//Example: https://ddragon.leagueoflegends.com/cdn/11.3.1/data/en_US/champion.json
const apiChampions = "/champion.json";

//Use: apiRiot + apiChampionsImage + urlImage
//Example: http://ddragon.leagueoflegends.com/cdn/img/champion/loading/Aatrox_0.jpg
const apiChampionsImage = "img/champion/loading/";

//Use: apiRiot + version + apiData + languaje + apiIconsData
//Example: https://ddragon.leagueoflegends.com/cdn/11.3.1/data/en_US/profileicon.json
const apiIconsData = "/profileicon.json";

//Use: apiRiot + version + apiData + apiIconsImage+ urlIcons
//Example: http://ddragon.leagueoflegends.com/cdn/11.3.1/img/profileicon/588.png
const apiIconsImage = "/img/profileicon/";

exports.manageRiot = async(req, res) => {
    var manager = req.params.manager;
    switch (manager) {
        case 'languaje':
            return getLanguaje(req, res);
            break;
        case 'version':
            return getVersion(req, res);
            break;
        case 'iconos':
            return getIconos(req, res);
            break;
        default:
            return false;
            break;
    }
};

var getLanguaje = async(req, res) => {
    request({
        url: apiLanguaje,
        json: false
    }, function(error, response, body) {
        if (!error && response.statusCode === 200) {
            res.json(body);
        }
    });
};

var getVersion = async(req, res) => {
    request({
        url: apiVersions,
        json: false
    }, function(error, response, body) {
        if (!error && response.statusCode === 200) {
            res.json(body);
        }
    });
};

var getIconos = async(req, res) => {
    let version = req.body.versionLeague;
    let languaje = req.body.languajeLeague;
    let apiUrl = apiRiot + version + apiData + languaje + apiIconsData;
    request({
        url: apiUrl,
        json: false
    }, function(error, response, body) {
        if (!error && response.statusCode === 200) {
            res.json(body);
        }
    });
};