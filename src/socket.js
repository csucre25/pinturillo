const {
    userJoin,
    getCurrentUser,
    userLeave,
    getRoomUsers
  } = require('./utils/users');

module.exports = io => {
    const BOT_IMG = "https://image.flaticon.com/icons/svg/327/327779.svg";
    const BOT_NAME = "BOT";
    // keep track of all lines that client sends
    var line_history = [];
    var message_history = [];
    var band_history = 0;
    io.on('connection', socket => {
        
        //salas
        socket.on('joinRoom', ({ username, room }) =>{
            const user = userJoin(socket.id, username, room);

            socket.join(user.room);

            
            socket.emit('send_message', { msg: "welcome to pinturift", token: BOT_NAME, time: null, image: BOT_IMG }); 

            // Broadcast cuando usuario se conecta
            socket.broadcast
            .to(user.room)
            .emit('send_message', { msg: `${username} joined the rift`, token: BOT_NAME, time: null, image: BOT_IMG });

            // enviar infomrmacion de la sala
            io.to(user.room).emit('roomUsers', {
                room: user.room,
                users: getRoomUsers(user.room)
            });
        });
                        
        //chat
        socket.on('send_message', data => {
            const user = getCurrentUser(socket.id);
            // message_history.push(data);
            if (user) {
                socket.broadcast
                .to(user.room).emit('send_message', { msg: data.msg, token: data.token, time: data.time, image: data.image });
            }
        });

        //pintar
        socket.on('clearCanvas', data => {
            line_history = [];
            console.log(line_history);
            io.emit('clearCanvas', {});
            band_history = 1;
        });

        socket.on('saveImage', data => {
            socket.broadcast.emit('saveImage', { image: data.image });
        });

        // for (let i in line_history) {

        //     console.log(line_history[i]);
        //     if (band_history == 1) {
        //         line_history = [];
        //         band_history = 0;
        //         break;
        //     }
        //     socket.broadcast.emit('draw_line', { line: line_history[i].line, thickness: line_history[i].thickness, color: line_history[i].color });
        // }

        socket.on('draw_line', data => {
            line_history.push(data);
            if (band_history == 1) {
                line_history = [];
                band_history = 0;
            }
            socket.broadcast.emit('draw_line', { line: data.line, thickness: data.thickness, color: data.color });
            // io.emit('draw_line', { line: data.line, thickness: data.thickness, color: data.color });
        });
        // for (let i in message_history) {

        //     io.emit('send_message', { msg: message_history[i].msg, token: message_history[i].token, time: message_history[i].time, image: message_history[i].image });
        // }


        //al salir

        socket.on('disconnect', () => {
            const user = userLeave(socket.id);
        
            if (user) {
              io.to(user.room).emit(
                'send_message', { msg: `${user.username} has left the chat`, token: BOT_NAME, time: null, image: BOT_IMG }
              );
        
              // Send users and room info
              io.to(user.room).emit('roomUsers', {
                room: user.room,
                users: getRoomUsers(user.room)
              });
            }
        });

    });

};