var image;
function clearCanvas() {
    ctx.clearRect(0, 0, 1400, 1000);
    socket.emit('clearCanvas', {});
}

// Saves image to bg

function saveImage() {
    image = c.toDataURL('image/png');
    socket.emit('saveImage', { image: image });
    $('.bg-img').css({
        'background-image': 'url(' + image + ')'
    });
}

// Draws a line

function drawLine(x, y) {
    ctx.lineTo(x, y);
    ctx.lineWidth = selectedThickness;
    ctx.strokeStyle = selectedColor;
    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    ctx.stroke();
    prevPoint = [x, y];
}

// Draws the initial picture

function mainLoop() {
    if (mouse.click && mouse.move && mouse.pos_prev) {
        socket.emit('draw_line', { line: [mouse.pos, mouse.pos_prev], thickness: selectedThickness, color: selectedColor });
        mouse.move = false;
    }
    mouse.pos_prev = { x: mouse.pos.x, y: mouse.pos.y };
    setTimeout(mainLoop, 15);
}

function eventos_socket_sala() {
    socket.on('draw_line', data => {
        let line = data.line;
        selectedThickness = data.thickness;
        selectedColor = data.color;
        console.log(data);
        ctx.moveTo(line[1].x, line[1].y);
        drawLine(line[0].x, line[0].y);
    });

    socket.on('clearCanvas', data => {
        ctx.clearRect(0, 0, 1400, 1000);
        saveImage();
    });
    socket.on('saveImage', data => {
        image = data.image;
        $('.bg-img').css({
            'background-image': 'url(' + image + ')'
        });
    });
    
}

function eventos_click_sala() {
    $('.color').on('click', function() {
        $('.color.active').removeClass('active');
        $(this).addClass('active');

        selectedColor = $(this).data('color');
    });

    $('.thickness').on('click', function() {
        $('.thickness.active').removeClass('active');
        $(this).addClass('active');

        selectedThickness = $(this).data('thickness');
    });

    // $('.pintor').on('click', function() {
    //     i_pintor = $('#pintortxt').val();
    //     console.log(i_pintor);
    // });
    $('[data-clear]').on('click', function() {
        clearCanvas();
        saveImage();
    });

    $('#save').on('click', function() {
        saveImage();
    });
}

function mov_mouse() {
    $(c).on('mousemove', function(e) {
        var x = e.offsetX * 2;
        var y = e.offsetY * 2;
        mouse.pos.x = x
        mouse.pos.y = y;

        mouse.move = isPressed;
        if (isPressed) {
            drawLine(x, y);
        }
    });

    $(c).on('mousedown', function(e) {
        prevPoint = [(e.offsetX * 2), e.offsetY * 2];
        ctx.beginPath();
        ctx.moveTo(prevPoint[0], prevPoint[1]);
        isPressed = true;
        mouse.click = true;
    });

    $(c).on('mouseup mouseleave', function() {
        isPressed = false;
        mouse.click = false;
        ctx.closePath();
        saveImage();
    });
}

function init() {
    c = $('#canvas')[0];
    ctx = c.getContext('2d');
    c.width = 1400;
    c.height = 1000;
    eventos_socket_sala();
    eventos_click_sala();
    mov_mouse();
    mainLoop();
}
