const msgerForm = get(".msger-inputarea");
const msgerInput = get(".msger-input");
const msgerChat = get(".msger-chat");

const BOT_MSGS = [
    "Hi, how are you?",
    "Ohh... I can't understand what you trying to say. Sorry!",
    "I like to play games... But I don't know how to play!",
    "Sorry if my answers are not relevant. :))",
    "I feel sleepy! :("
];
var chatroom = 0;
/*
Logica para uso de tokens en sesion, uso de iconos, divisiones y perfil
*/
// Icons made by Freepik from www.flaticon.com


function initMessage() {
    msgerForm.addEventListener("submit", event => {
        event.preventDefault();

        const msgText = msgerInput.value;
        if (!msgText) return;
        let time = new Date();
        let hora = time.getHours();
        let minuto = time.getMinutes();
        let segundo = time.getSeconds();
        let timeMessage = hora + " : " + minuto + " : " + segundo;
        appendMessage(username, PERSON_IMG, "right", msgText);
        socket.emit('send_message', { msg: msgText, token: username, time: timeMessage, image: PERSON_IMG });
        msgerInput.value = "";
    });
    socket.on('send_message', data => {
        appendMessage(data.token, data.image, "left", data.msg);
    });
}



function appendMessage(name, img, side, text) {
    //   Simple solution for small apps
    const msgHTML = `
    <div class="msg ${side}-msg">
      <div class="msg-img" style="background-image: url(${img})"></div>

      <div class="msg-bubble">
        <div class="msg-info">
          <div class="msg-info-name">${name}</div>
          <div class="msg-info-time">${formatDate(new Date())}</div>
        </div>

        <div class="msg-text">${text}</div>
      </div>
    </div>
  `;

    msgerChat.insertAdjacentHTML("beforeend", msgHTML);
    msgerChat.scrollTop += 500;
}

// Utils
function get(selector, root = document) {
    return root.querySelector(selector);
}

function formatDate(date) {
    const h = "0" + date.getHours();
    const m = "0" + date.getMinutes();

    return `${h.slice(-2)}:${m.slice(-2)}`;
}

function random(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

