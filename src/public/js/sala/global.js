//Constants for riot api
const apiRiot = "http://ddragon.leagueoflegends.com/cdn/";
const apiVersions = "https://ddragon.leagueoflegends.com/api/versions.json";
const apiLanguaje = "https://ddragon.leagueoflegends.com/cdn/languages.json";
const apiData = "/data/";

//Use: apiRiot + version + apiData + languaje + "/champion.json"
//Example: http://ddragon.leagueoflegends.com/cdn/11.3.1/data/en_US/champion.json
const apiChampions = "/champion.json";

//Use: apiRiot + apiChampionsImage + urlImage
//Example: http://ddragon.leagueoflegends.com/cdn/img/champion/loading/Aatrox_0.jpg
const apiChampionsImage = "img/champion/loading/";

//Use: apiRiot + version + apiData + languaje + apiIconsData
//Example: http://ddragon.leagueoflegends.com/cdn/11.3.1/data/en_US/profileicon.json
const apiIconsData = "/profileicon.json";

//Use: apiRiot + version + apiData + apiIconsImage+ urlIconss
//Example: http://ddragon.leagueoflegends.com/cdn/11.3.1/img/profileicon/588.png
const apiIconsImage = "/img/profileicon/";


// Socket IO
const socket = io();
//nombre pintor
var i_pintor;


var c;
var ctx ;
var selectedColor = '#1abc9c';
var selectedThickness = 4;
var selectedLayer = 1;
var isPressed = false;
var prevPoint = [0, 0];
var mouse = {
    click: false,
    move: false,
    pos: { x: 0, y: 0 },
    pos_prev: false
};



//chat
const BOT_IMG = "https://image.flaticon.com/icons/svg/327/327779.svg";
const PERSON_IMG = "https://image.flaticon.com/icons/svg/145/145867.svg";
const BOT_NAME = "BOT";
// const PERSON_NAME = "Jean";

var chatroom = 0;

//usuario y room
// Get username and room from URL
const { username, room } = Qs.parse(location.search, {
    ignoreQueryPrefix: true,
});