const userNumbers = $('#usersNumber');
const listUsershtml = $('#usersList');
function listUsers(users) {
    //   Simple solution for small apps
    let msgHTML = "";
    for (let index = 0; index < users.length; index++) {
        msgHTML += `<div class="summoner">
                     <div class="summoner-img" style="background-image: url(${PERSON_IMG})"></div>
                        <div class="summoner-name">
                            <label>
                                ${users[index].username}
                            </label> 
                        </div>
                    </div>
            `;
    }
    console.log(msgHTML);
    listUsershtml.html(msgHTML);
    //listUsershtml.scrollTop += 500;
}

function initUsers(params) {
    socket.on('roomUsers', data => {
        let usersNumber = data.users.length;
        userNumbers.text(usersNumber);
        listUsers(data.users);
        // console.log(data);
    });    
}