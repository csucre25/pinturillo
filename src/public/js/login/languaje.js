let version = 1.1;
const prefixRiot = "https://ddragon.leagueoflegends.com/cdn/";
const prefixData = "/data/";
const prefixIconsImage = "/img/profileicon/";
const lang = navigator.language || navigator.userLanguage;
$("#loading").show("fade", { direction: "up" }, "slow");

function getLanguaje() {
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "../riot/languaje",
        headers: {
            //'Authorization': 'Bearer ' + btoa(API_TOKEN),
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin": "*"
        },
        crossDomain: true,
        beforeSend: function() {
            //$("#loading").show("fade", { direction: "up" }, "slow");
        },
        complete: function() {
            //$("#loading").hide("fade", { direction: "up" }, "slow");
        },
        success: function(data) {
            var obj = JSON.parse(data);
            $("#cboLanguaje").empty();
            obj.forEach(function(element) {
                let languaje = element.replace("_", "-");
                let langDefault = lang.replace("-", "_");
                let languageNames = new Intl.DisplayNames([lang], { type: 'language' });
                $("#cboLanguaje").append('<option value="' + element + '">' + languageNames.of(languaje) + '</option>');
                if (langDefault == element) {
                    $("#cboLanguaje").val(langDefault);
                }

            });

            getIconos();

        },
        error: function(data) {
            $("#loading").hide("fade", { direction: "up" }, "slow");
            alert('Error al recuperar languaje');
        }
    });
}

function getIconos() {
    let languaje = $("#cboLanguaje").val();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "../riot/version",
        headers: {
            //'Authorization': 'Bearer ' + btoa(API_TOKEN),
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin": "*"
        },
        crossDomain: true,
        beforeSend: function() {
            //$("#loading").show("fade", { direction: "up" }, "slow");
        },
        complete: function() {
            // $("#loading").hide("fade", { direction: "up" }, "slow");
        },
        success: function(data) {
            var obj = JSON.parse(data);
            // Se recupera la ultima versión
            version = obj[0];
            $.ajax({
                type: 'POST',
                dataType: "json",
                data: JSON.stringify({ versionLeague: version, languajeLeague: languaje }),
                url: "../riot/iconos",
                headers: {
                    'Content-Type': 'application/json',
                    "Access-Control-Allow-Origin": "*"
                },
                crossDomain: true,
                beforeSend: function() {
                    // $("#loading").show("fade", { direction: "up" }, "slow");
                },
                complete: function() {
                    //$("#loading").hide("fade", { direction: "up" }, "slow");
                },
                success: function(dataIcon) {
                    const content = document.querySelector("#olIconos");
                    content.innerHTML = "";
                    let oIcon = JSON.parse(dataIcon);
                    olIcons = oIcon.data;
                    olIconsId = Object.keys(olIcons);
                    for (var i = olIconsId.length - 2; i > olIconsId.length - 106; i--) {
                        let url = prefixRiot + oIcon.version + prefixIconsImage + olIcons[olIconsId[i]].image.full;
                        content.innerHTML += '<img id="icon' + i + '" class="imgIconoList" src="' + url + '" onClick="setUrlIcon(' + "'" + "icon" + i + "','" + url + "'" + ')" alt="">';
                        if (i == olIconsId.length - 105) {
                            $("#loading").hide("fade", { direction: "up" }, "slow");
                        }
                    }
                },
                error: function(data) {
                    $("#loading").hide("fade", { direction: "up" }, "slow");
                    alert('Error al recuperar iconos');
                }
            });
        },
        error: function(data) {

            var obj = JSON.parse(data);
            // Se recupera la ultima versión
            console.log(obj);
            $("#loading").hide("fade", { direction: "up" }, "slow");
            alert('Error al recuperar versions');
        }
    });

    //$.when(ajaxVersion).done(function(data) {

    //});

}

function setUrlIcon(idIcon, url) {
    $(".imgIconoList").removeClass("selected");
    $("#" + idIcon).addClass("selected");
    urlImage = url;
}

$("#selectIcon").on("click", function() {
    if (urlImage != "") {
        $("#selectIcono1").attr("src", urlImage);
        $("#selectIcono2").attr("src", urlImage);
    }
});

$(document).ready(function() {
    getLanguaje();
    xStorage = window.localStorage;

    $("#selectIcono1").on("click", function() {
        $("#staticBackdrop").modal("show");
    });

    $("#selectIcono2").on("click", function() {
        $("#staticBackdrop").modal("show");
    });

    $("#btnCreate").on("click", function() {
        let url_Icon = $("#selectIcono1").attr("src");
        let user_name = $("#txtUsername1").val();
        let option = "Created";
        let capacidad = $("#txtCapacidad").val();
        $.ajax({
            type: 'POST',
            dataType: "json",
            data: JSON.stringify({ url_Icon: url_Icon, user_name: user_name, room_code: 0, option: option, capacidad: capacidad }),
            url: "../user/create",
            headers: {
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*"
            },
            crossDomain: true,
            beforeSend: function() {
                $("#loading").show("fade", { direction: "up" }, "slow");
            },
            complete: function() {
                //$("#loading").hide("fade", { direction: "up" }, "slow");
            },
            success: function(data) {
                $("#loading").hide("fade", { direction: "up" }, "slow");
                console.log(data);
            },
            error: function(data) {
                $("#loading").hide("fade", { direction: "up" }, "slow");
                alert('Error al ingresar datos');
            }
        });
    });

    $("#btnJoin").on("click", function() {
        let url_Icon = $("#selectIcono2").attr("src");
        let user_name = $("#txtUsername2").val();
        let room_code = $("#txtCodeRoom").val();
        let option = "Joined";
        if (url_Icon == undefined || url_Icon == "" || url_Icon == null) {
            return false;
        }
        if (user_name == undefined || user_name == "" || user_name == null) {
            return false;
        }
        $.ajax({
            type: 'POST',
            dataType: "json",
            data: JSON.stringify({ url_Icon: url_Icon, user_name: user_name, room_code: room_code, option: option }),
            url: "../user/create",
            headers: {
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*"
            },
            crossDomain: true,
            beforeSend: function() {
                $("#loading").show("fade", { direction: "up" }, "slow");
            },
            complete: function() {
                //$("#loading").hide("fade", { direction: "up" }, "slow");
            },
            success: function(data) {
                $("#loading").hide("fade", { direction: "up" }, "slow");
                console.log(data);
            },
            error: function(data) {
                $("#loading").hide("fade", { direction: "up" }, "slow");
                alert('Error al ingresar datos');
            }
        });
    });

});