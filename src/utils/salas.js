const salas = [];

// nueva sala
function addRoom(id, capacidad) {
    const sala = { id, capacidad };
  
    salas.push(sala);
  
    return sala;
}

//obtener la ultima sala
function getMaxRoom() {
    if (salas[salas.length -1]) {
        return salas[salas.length -1];
    }else{
        return null;
    }
}

//obtener salas
function getRooms() {
    return salas;
}

module.exports = {
    addRoom,
    getMaxRoom,
    getRooms
};