const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const https = require('https');
const partials = require('express-partials');
const socketIO = require('socket.io');
const path = require('path');
const uuidv4 = require("uuid");
var jwt = require('jsonwebtoken');
var firebase = require('firebase');
var admin = require("firebase-admin");
var request = require("request");
var serviceAccount = require("./pinturillov2-firebase-adminsdk-7ag89-54c3265fc7.json");
// intializations
const app = express();
const server = http.createServer(app);
const io = socketIO(server);

salas = require('./utils/salas');

// settings
app.set('port', process.env.PORT || 3000);

// sockets
require('./socket')(io);

//Rutas
var user = require('./routes/user');
var riot = require('./routes/riot');

// static files
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(function(req, res, next) {

    res.setHeader("Access-Control-Allow-Origin", "*");

    res.header("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");

    res.header("Access-Control-Max-Age", "3600");

    res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    next();

});
app.use(partials());


app.get('/home*', (req, res) => {
    res.render('sala/index', {
        estilos: ['style', 'chat', 'library/bootstrap/css/bootstrap'],
        scripts: ['library/bootstrap/js/bootstrap', 'sala/global', 'sala/index', 'sala/chat', 'sala/usuarios', 'sala/iniciar']
    });
});

app.get('/', (req, res) => {
    res.render('login/login', {
        estilos: ['login', 'loader', 'library/bootstrap/css/bootstrap'],
        scripts: ['login/languaje', 'login/login', 'library/bootstrap/js/bootstrap']
    });
});

app.use('/user', user);

app.use('/riot', riot);

// starting the server
server.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});